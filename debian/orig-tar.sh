#!/bin/sh -e

# $2 version
TAR=../antelope_$2.orig.tar.gz
DIR=antelope-$2.orig

# clean up the upstream tarball $3
unzip $3
mv Antelope* $DIR
GZIP=--best tar -c -z -f $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

exit 0
