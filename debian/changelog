antelope (3.5.1-5) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with OpenJDK 17 (Closes: #981957)
  * Standards-Version updated to 4.5.1
  * Switch to debhelper level 13
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 11 Feb 2021 09:49:27 +0100

antelope (3.5.1-4) unstable; urgency=medium

  * Team upload.

  [ Chris West (Faux) ]
  * Fixed a build failure with Java 9. (Closes: #873206)
    See 03_fix_javadoc_error.diff.

  [ Markus Koschany ]
  * Switch to compat level 10.
  * Declare compliance with Debian Policy 4.1.1.
  * wrap-and-sort -sa.
  * Use canonical Vcs address.
  * Migrate from CDBS to DH sequencer.

 -- Markus Koschany <apo@debian.org>  Tue, 24 Oct 2017 18:39:42 +0200

antelope (3.5.1-3) unstable; urgency=medium

  * Team upload.
  * Fixed a build failure with Java 8 (Closes: #748039)
  * debian/control:
    - Co-maintenance with the Java Team
    - Added the Vcs-* fields
    - Standards-Version updated to 3.9.6 (no changes)
  * debian/copyright: Updated to the Copyright Format 1.0
  * debian/rules: Improved the clean target

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 11 Dec 2014 15:34:12 +0100

antelope (3.5.1-2) unstable; urgency=low

  * Fix FTBFS during docs XSL transformation:
    - d/rules: Use build.xml instead of build-tasks.xml (to use xsltproc).
    - d/control: Drop B-D on ant-optional and xalan2-java, add B-D on xsltproc.
    - d/patches/01_also_build_javadoc.diff: Build JAR and Javadoc.
    - d/patches/02_xsltproc_opts.diff: Pass --nonet to xsltproc.

 -- Damien Raude-Morvan <drazzib@debian.org>  Mon, 16 Aug 2010 18:49:39 +0200

antelope (3.5.1-1) unstable; urgency=low

  * Initial release. (Closes: #589758)

 -- Damien Raude-Morvan <drazzib@debian.org>  Thu, 05 Aug 2010 18:30:05 +0200
